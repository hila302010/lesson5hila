#include "Point.h"

Point::Point()
{
}
// constructor with intilazation line
Point::Point(double x, double y) :
	_x(x), _y(y)
{
	_x = x;
	_y = y;
}
// ctor
Point::Point(const Point & other)
{
	_x = other._x;
	_y = other._y;
}
// dtor
Point::~Point()
{
}

Point Point::operator+(const Point & other) const
{
	Point p;
	p._x = _x + other._x;
	p._y = _y + other._y;
	return p;
}

Point & Point::operator+=(const Point & other)
{
	_x += other._x;
	_y += other._y;
	return *this;
}

// getters
double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point & other) const
{
	return sqrt(pow(_y - other._y, 2) - pow(_x - other._x, 2));
}
