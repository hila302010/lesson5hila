#pragma once
#include <iostream>
#include "Point.h"
#include "CImg.h"

using namespace std;

class Shape 
{
public:
	// constructors
	Shape();
	Shape(const string& name, const string& type);
	// pure virtual functions
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0;
	virtual void move(const Point& other) = 0; // add the Point to all the points of shape
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0;

	// getters
	string getType() const;
	string getName() const;
	// setters
	void setType(const string & type);
	void setName(const string & name);

private:
	string _name;
	string _type;

protected:
	std::vector<cimg_library::CImg<unsigned char>> _graphicShape;
};