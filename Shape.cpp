#include "Shape.h"

Shape::Shape()
{
	_name = "";
	_type = "";
}
// constructor with initialization line
Shape::Shape(const string & name, const string & type) :
	_name{ name } , _type{ type }
{
	_name = name;
	_type = type;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return _name;
}

void Shape::setType(const string & type)
{
	_type = type;
}

void Shape::setName(const string & name)
{
	_name = name;
}

