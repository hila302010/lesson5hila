#pragma once
#include "Polygon.h"

namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	private:
		Point _p1;
		Point _p2;
		double _length;
		double _width;
	public:
		// There's a need only for the top left corner
		// ctors
		Rectangle();
		Rectangle(const Point& a, double length, double width, const string& type, const string& name);
		// dtor
		virtual ~Rectangle();
		// override functions if need (virtual + pure virtual)
		virtual double getArea() const;
		virtual double getPerimeter()const;
		virtual void move(const Point& other);
		void draw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);
		void clearDraw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);

	};
}