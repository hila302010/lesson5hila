#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
private:
	Point _p1;
	Point _p2;
	Point _p3;
	string _name;
	string _type;
public:
	// ctors
	Triangle();
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	// dtor
	virtual ~Triangle();
	// override functions if need (virtual + pure virtual)
	virtual double getArea() const;
	virtual double getPerimeter()const;
	virtual void move(const Point& other);
	virtual void draw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);
	
};