#pragma once
#include <vector>
#include "CImg.h"

class Point
{
private:
	double _x;
	double _y;
public:
	// constructors
	Point();
	Point(double x, double y);
	Point(const Point& other);
	// dtor
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);
	// getters
	double getX() const;
	double getY() const;

	double distance(const Point& other) const;
	
};