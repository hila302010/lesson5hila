#include "Rectangle.h"

// ctor
myShapes::Rectangle::Rectangle()
{
}
// constructor with intilazation line
myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	_p1 = a;
	_p2 = Point(a.getX() + length, a.getY() + width);
	_length = length;
	_width = width;
}
// dtor
myShapes::Rectangle::~Rectangle()
{
}
// getters
double myShapes::Rectangle::getArea() const
{
	return _length * _width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return (_length * 2) + (_width * 2);
}
// modify
void myShapes::Rectangle::move(const Point & other)
{
	_p1 += other;
	_p2 += other;
}
// draw
void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), WHITE, 100.0f).display(disp);
}
// clear the shape - board comes black
void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}