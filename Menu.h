#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
enum Shapes { circle, arrow, triangle, rectangle };
#define EXIT 3

using namespace std;
class Menu
{
public:
	// ctor
	Menu();
	// dtor
	~Menu();
	int printMenu();
	void printShapesMenu();
	// option 1 - create a shape
	void checkNewShape(vector<Shape*>& shapes); 
	// a new shape:
	void createCircle(vector<Shape*>& shapes); // circle
	void createArrow(vector<Shape*>& shapes); // arrow
	void createTriangle(vector<Shape*>& shapes); // triangle
	void createRectangle(vector<Shape*>& shapes); // rectangle
	// option 2 - move / get information 
	unsigned short printAllShapes(vector<Shape*>& shapes);
	void getInformation(vector<Shape*>& shapes, unsigned short index);
	void moveShape(vector<Shape*>& shapes, unsigned short choice);
	void removeShape(vector<Shape*>& shapes, unsigned short choice, bool deleted);
	// option 3 - delete all shapes
	void deleteShapes(vector<Shape*>& shapes); 

private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

