#include "Menu.h"
// constructor 
Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}
// dtor
Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

int Menu::printMenu()
{
	int choice;
	// print the menu
	printf("Enter 0 to add a new shape.\n");
	printf("Enter 1 to modify or get information from a current shape.\n");
	printf("Enter 2 to delete all of the shapes.\n");
	printf("Enter 3 to exit.\n");
	//indox the choice: must be 0-3
	do {
		printf("Enter: ");
		scanf("%d", &choice);
		getchar();
	} while (choice < 0 || choice > EXIT);
	system("cls");
	return choice;
	
}

void Menu::printShapesMenu()
{
	printf("Enter 0 to add a circle.\n");
	printf("Enter 1 to add an arrow.\n");
	printf("Enter 2 to add a triangle.\n");
	printf("Enter 3 to add a rectangle.\n");
}

void Menu::checkNewShape(vector<Shape*>& shapes)
{
	//indox the choice: must be 0-3
	unsigned short choice;
	do {
		printf("Enter: ");
		cin >> choice;
		getchar();
	} while (choice < 0 || choice > 3);
	switch (choice)
	{
	case circle: // 0
		createCircle(shapes);
		break;
	case arrow: // 1
		createArrow(shapes);
		break;
	case triangle: // 2
		createTriangle(shapes);
		break;
	case rectangle: // 3
		createRectangle(shapes);
	default:
		break;
	}
	system("cls");
}

// create a circle
void Menu::createCircle(vector<Shape*>& shapes)
{
	double x, y, rad;
	string name;
	printf("Please enter X: ");
	cin >> x;
	getchar();
	printf("Please enter Y: ");
	cin >> y;
	getchar();
	printf("Please enter radius: ");
	cin >> rad;
	getchar();
	printf("Please enter the name of the shape: ");
	cin >> name;
	getchar();
	Circle* c = new Circle(Point(x, y), rad, "Circle", name);
	shapes.push_back(c);
	c->draw(*this->_disp, *this->_board);
}

// create an arrow
void Menu::createArrow(vector<Shape*>& shapes)
{
	double xp1, yp1, xp2, yp2;
	string name;
	printf("Enter the X of point number: 1\n");
	cin >> xp1;
	getchar();
	printf("Enter the Y of point number: 1\n");
	cin >> yp1;
	getchar();
	printf("Enter the X of point number: 2\n");
	cin >> xp2;
	getchar();
	printf("Enter the Y of point number: 2\n");
	cin >> yp2;
	getchar();
	printf("Please enter the name of the shape: ");
	cin >> name;
	getchar();
	Arrow* arrow = new Arrow(Point(xp1, yp1), Point(xp2, yp2), "Arrow", name);
	shapes.push_back(arrow);
	arrow->draw(*this->_disp, *this->_board);
}

// create a triangle
void Menu::createTriangle(vector<Shape*>& shapes)
{
	double xp1, yp1, xp2, yp2, xp3, yp3;
	string name;
	printf("Enter the X of point number: 1\n");
	cin >> xp1;
	getchar();
	printf("Enter the Y of point number: 1\n");
	cin >> yp1;
	getchar();
	printf("Enter the X of point number: 2\n");
	cin >> xp2;
	getchar();
	printf("Enter the Y of point number: 2\n");
	cin >> yp2;
	getchar();
	printf("Enter the X of point number: 3\n");
	cin >> xp3;
	getchar();
	printf("Enter the Y of point number: 3\n");
	cin >> yp3;
	getchar();
	printf("Please enter the name of the shape: ");
	cin >> name;
	getchar();
	// the triangle can't be on one line - else it's not a triangle
	if ((xp1 == xp2 && xp1 == xp3) || (yp1 == yp2 && yp1 == yp3)) {
		cout << "your points create a line:( triangle won't be created.\n";
		getchar();
	}
	else {
		Triangle* t = new Triangle(Point(xp1, yp2), Point(xp2, yp2), Point(xp3, yp3), "Triangle", name);
		shapes.push_back(t);
		t->draw(*this->_disp, *this->_board);
	}
}

// create a rectangle
void Menu::createRectangle(vector<Shape*>& shapes)
{
	double x, y, len, wid;
	string name;
	printf("Enter the X of the to left corner:");
	cin >> x;
	getchar();
	printf("Enter the Y of the to left corner: ");
	cin >> y;
	getchar();
	printf("Enter the lenght of the shape ");
	cin >> len;
	getchar();
	printf("Enter the width of the shape ");
	cin >> wid;
	getchar();
	printf("Please enter the name of the shape: ");
	cin >> name;
	getchar();
	// rectangle can't have length 0 or width 0
	if (len == 0 || wid == 0) {
		cout << "length or width can't be 0:( rectagle won't be created.";
		getchar();
	}
	else {
		myShapes::Rectangle* r = new myShapes::Rectangle(Point(x, y), len, wid, "Rectangle", name);
		shapes.push_back(r);
		r->draw(*this->_disp, *this->_board);
	}
}

// print the shapes from the vector
unsigned short Menu::printAllShapes(vector<Shape*>& shapes)
{
	unsigned short choice;
	// if the vector is empty: nothing to print
	if (shapes.size() == 0)
		return 0;
	else {
		do // loop on the vector of the shapes
		{
			system("cls");
			for (int i = 0; i < shapes.size(); i++) {
				cout << "Enter " << i << " for " << shapes[i]->getName() <<
					"(" << shapes[i]->getType() << ")" << endl;
			}
			cin >> choice;
			getchar();
		} while (choice < 0 || choice >= shapes.size());
		return choice;// chosen shape index
	}
}

// option 2 from the main menu - motivate or get information about a shape
void Menu::getInformation(vector<Shape*>& shapes, unsigned short index)
{
	int choice;
	cout << "\nEnter 0 to move the shape" << endl;
	cout << "Enter 1 to get its details." << endl;
	cout << "Enter 2 to remove the shape." << endl;
	do {
		cout << "Enter: ";
		cin >> choice;
		getchar();
	} while (choice < 0 || choice > 2);
	switch (choice)
	{
	case 0:
		moveShape(shapes, index);
		break;
	case 1:
		cout << shapes[index]->getType() << "  " << shapes[index]->getName() << "  ";
		cout << shapes[index]->getArea() << "  " << shapes[index]->getPerimeter()<<endl;
		getchar();
		break;
	case 2:
		removeShape(shapes, index, true);
		break;
	default:
		break;
	}
	system("cls");
}
// option 1 from option 2 in the main menu
void Menu::moveShape(vector<Shape*>& shapes, unsigned short choice)
{
	system("cls");
	double x, y;
	cout << "Please enter the X moving scale: ";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;

	removeShape(shapes, choice, false);
	shapes[choice]->move(Point(x, y));
	shapes[choice]->draw(*_disp, *_board);
}

// option 3 from option 2 in the main menu
void Menu::removeShape(vector<Shape*>& shapes, unsigned short choice, bool deleted)
{
	// first delete
	for (int i = 0; i < shapes.size(); i++)
	{
		if (i == choice)
		{
			shapes[i]->clearDraw(*_disp, *_board);
		}
	}
	// then draw the rest
	for (int i = 0; i < shapes.size(); i++)
	{
		if (i != choice)
		{
			shapes[i]->draw(*_disp, *_board);
		}
	}

	if (deleted)
		shapes.erase(shapes.begin() + choice);
}
// option 3 from the main menu - clear all shapes
void Menu::deleteShapes(vector<Shape*>& shapes)
{
	for (int i = 0; i < shapes.size(); i++) {
		shapes[i]->clearDraw(*_disp, *_board);
	}
	shapes.clear();
	cout << "shapes were deleted\n\n";
}


