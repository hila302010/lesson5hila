#include "Menu.h"

int main()
{
	vector<Shape*> shapes;
	Menu newMenu;
	unsigned short choice, index;
	do {
		// print the menu and indox the choice
		choice = newMenu.printMenu();
		switch (choice)
		{
		case 0: // option 1 - create new shape
			newMenu.printShapesMenu();
			newMenu.checkNewShape(shapes);
			break;
		case 1: // option 2 - get data / move shape
			index = newMenu.printAllShapes(shapes);
			if (shapes.size() > 0)
				newMenu.getInformation(shapes, index);
			break;
		case 2: // option 3 - delete all shapes
			newMenu.deleteShapes(shapes);
			break;
		default:
			break;
		}
	} while (choice != EXIT);

	getchar();
	return 0;
}