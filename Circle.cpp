#include "Circle.h"

// constructor with intilization line
Circle::Circle(const Point & center, double radius, const string & type, const string & name) :
	Shape {type, name}
{
	_center = center;
	_radius = radius;
}

// dtor
Circle::~Circle()
{
}

const Point & Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}
// area
double Circle::getArea() const
{
	return ((this->getRadius()) * (this->getRadius()) * PI);
}
// perimeter
double Circle::getPerimeter() const
{
	return (2 * PI * this->getRadius());
}
// modify
void Circle::move(const Point & other)
{
	_center += other;
}
// draw
void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}
// delete
void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}


