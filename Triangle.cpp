#include "Triangle.h"
// ctor
Triangle::Triangle()
{
}
// constructor
Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name) : Polygon(type, name)
{
	_p1 = a;
	_p2 = b;
	_p3 = c;
	_type = type;
	_name = name;
}
// dtor
Triangle::~Triangle()
{
}
// getters
double Triangle::getArea() const
{
	double a, b, c, s, area;
	a = _p1.distance(_p2);
	b = _p2.distance(_p3);
	c = _p3.distance(_p1);
	s = (a + b + c) / 2;
	area = sqrt(s * (s - a)* (s - b) *(s - c));
	return area;

}

double Triangle::getPerimeter() const
{
	double a, b, c;
	a = _p1.distance(_p2);
	b = _p2.distance(_p3);
	c = _p3.distance(_p1);
	return a + b + c;

}
// modify
void Triangle::move(const Point & other)
{
	_p1 += other;
	_p2 += other;
	_p3 += other;
}
// draw the shape
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(),
		_p3.getX(), _p3.getY(), GREEN, 100.0f).display(disp);
}
// delete the shape - draw the board in black
void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(),
		_p3.getX(), _p3.getY(), BLACK, 100.0f).display(disp);
}

